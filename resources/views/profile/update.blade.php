@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Update User Profile</div>

        <div class="panel-body">
            <div class="row">
                <form  class="form-horizontal" action="{{ route('update_profile') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="country" class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name',  isset($user->name) ? $user->name : null ) }}" required >

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('default_from_address') ? ' has-error' : '' }}">
                        <label for="country" class="col-md-4 control-label">Default From Address</label>

                        <div class="col-md-6">
                            <select name="default_from_address" id="default_from_address" class="col-md-4 control-label">
                                <option value="">Select Address</option>
                                @foreach($userAddressList as $id=>$title)
                                    <option value="{{ $id }}" {{ ($id == $user->default_from_address) ? 'selected' : '' }}>{{ $title}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('default_from_address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('default_from_address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('default_to_address') ? ' has-error' : '' }}">
                        <label for="country" class="col-md-4 control-label">Default To Address</label>

                        <div class="col-md-6">
                            <select name="default_to_address" id="default_to_address" class="col-md-4 control-label">
                                <option value="">Select Address</option>
                                @foreach($userAddressList as $id=>$title)
                                    <option value="{{ $id }}" {{ ($id == $user->default_to_address) ? 'selected' : '' }}>{{ $title}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('default_to_address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('default_to_address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
