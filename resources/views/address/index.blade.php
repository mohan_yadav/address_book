@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading"><span>Address Book </span>
            <div class="pull-right">
                <a href="{{ route('add_new_address') }}" class="btn btn-primary btn-sm ">Add New</a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-body">
            <div class="row">
                @if($addresses->count())
                    @include('address.address_list')
                @else
                    <div class="panel-body">
                        No Address in your Address Book !!
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
