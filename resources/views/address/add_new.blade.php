@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading"><span>Address Book </span>
            <div class="pull-right">
                <a href="{{ route('add_new_address') }}" class="btn btn-primary btn-sm ">Add New</a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-body">
            <div class="row">
                <form  class="form-horizontal" action="{{ (isset($address)) ? route('update_address',['address'=>$address->id]) : route('save_address') }}" method="POST">
                    @include('address._form')
                </form>
            </div>
        </div>
    </div>
@endsection
