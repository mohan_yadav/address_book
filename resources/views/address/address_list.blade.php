@foreach($addresses as $address)
    <div class="col-md-4">
        <div class="card card-block">
            <h3 class="card-title">{{ $address->title }}</h3>
            <address class="card-text">
                <strong>{{ $address->contact_person_name }}</strong><br>
                <strong>Contact No: {{ $address->contact_person_number }}</strong><br>
                {{ $address->address_line_1 }}<br>
                {!! ($address->address_line_2 != '') ? $address->address_line_2."<br>" : "" !!}
                {!! ($address->address_line_3 != '') ? $address->address_line_3."<br>" : "" !!}
                {{  $address->city.", ".$address->state }}<br>
                {{  $address->country." ".$address->pin_code }}<br>
            </address>
            <a href="{{ route('edit_address', ['address'=>$address->id]) }}" class="btn btn-primary">Update</a>
            <a href="{{ route('delete_address', ['address'=>$address->id]) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this address ?')">Delete</a>
        </div>
    </div>
@endforeach