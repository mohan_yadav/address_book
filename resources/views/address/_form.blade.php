{{ csrf_field() }}

<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Title</label>

    <div class="col-md-6">
        <input id="title" type="text" class="form-control" name="title" value="{{ old('title', isset($address->title) ? $address->title : null ) }}"  autofocus>

        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('contact_person_name') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Contact Person Name</label>

    <div class="col-md-6">
        <input id="contact_person_name" type="text" class="form-control" name="contact_person_name" value="{{ old('contact_person_name', isset($address->contact_person_name) ? $address->contact_person_name : null ) }}"  >

        @if ($errors->has('contact_person_name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('contact_person_name') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('contact_person_number') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Contact Person Number</label>

    <div class="col-md-6">
        <input id="contact_person_number" type="text" class="form-control" name="contact_person_number" value="{{ old('contact_person_number', isset($address->contact_person_number) ? $address->contact_person_number : null ) }}"  >

        @if ($errors->has('contact_person_number'))
            <span class="help-block">
                                    <strong>{{ $errors->first('contact_person_number') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
    <label for="address_line_1" class="col-md-4 control-label">Address Line 1</label>

    <div class="col-md-6">
        <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1', isset($address->address_line_1) ? $address->address_line_1 : null ) }}"  >

        @if ($errors->has('address_line_1'))
            <span class="help-block">
                                    <strong>{{ $errors->first('address_line_1') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
    <label for="address_line_2" class="col-md-4 control-label">Address Line 2</label>

    <div class="col-md-6">
        <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2', isset($address->address_line_2) ? $address->address_line_2 : null ) }}" >

        @if ($errors->has('address_line_2'))
            <span class="help-block">
                                    <strong>{{ $errors->first('address_line_2') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address_line_3') ? ' has-error' : '' }}">
    <label for="address_line_3" class="col-md-4 control-label">Address Line 3</label>

    <div class="col-md-6">
        <input id="address_line_3" type="text" class="form-control" name="address_line_3" value="{{ old('address_line_3', isset($address->address_line_3) ? $address->address_line_3 : null ) }}" >

        @if ($errors->has('address_line_3'))
            <span class="help-block">
                                    <strong>{{ $errors->first('address_line_3') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('pin_code') ? ' has-error' : '' }}">
    <label for="pin_code" class="col-md-4 control-label">Pincode</label>

    <div class="col-md-6">
        <input id="pin_code" type="text" class="form-control" name="pin_code" value="{{ old('pin_code', isset($address->pin_code) ? $address->pin_code : null ) }}"  >

        @if ($errors->has('pin_code'))
            <span class="help-block">
                <strong>{{ $errors->first('pin_code') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label for="city" class="col-md-4 control-label">City</label>

    <div class="col-md-6">
        <input id="city" type="text" class="form-control" name="city" value="{{ old('city',  isset($address->city) ? $address->city : null ) }}"  >

        @if ($errors->has('city'))
            <span class="help-block">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
    <label for="state" class="col-md-4 control-label">State</label>

    <div class="col-md-6">
        <input id="state" type="text" class="form-control" name="state" value="{{ old('state',  isset($address->state) ? $address->state : null ) }}"  >

        @if ($errors->has('state'))
            <span class="help-block">
                                    <strong>{{ $errors->first('state') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    <label for="country" class="col-md-4 control-label">Country</label>

    <div class="col-md-6">
        <input id="country" type="text" class="form-control" name="country" value="{{ old('country',  isset($address->country) ? $address->country : null ) }}"  >

        @if ($errors->has('country'))
            <span class="help-block">
                                    <strong>{{ $errors->first('country') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            {{ isset($address) ? 'Update' : 'Save' }}
        </button>
    </div>
</div>
