<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultAddressFieldInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->integer('default_from_address')->unsigned()->nullable()->after('password');
            $table->integer('default_to_address')->unsigned()->nullable()->after('default_from_address');

            $table->foreign('default_from_address')->references('id')->on('addresses')->onDelete('set null');
            $table->foreign('default_to_address')->references('id')->on('addresses')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign('users_default_from_address_foreign');
            $table->dropForeign('users_default_to_address_foreign');

            $table->dropColumn('default_from_address');
            $table->dropColumn('default_to_address');
        });
    }
}
