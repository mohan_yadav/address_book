## Address Book

This is a simple address storage web application built with laravel  framework. Users can register and save addresses. A user can also set a default from and to address.


## Server Requirements
- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Deployment Instructions
- Open your linux terminal and clone this repository using the command below:
```
$ git clone https://mohan_yadav@bitbucket.org/mohan_yadav/address_book.git
```
- cd into the address_book directory
```
$ cd address_book
```
- run the command to download laravel system files from composer
```
$ composer install
```
- Make sure that storage and bootstrap/cache directories are writable.
- Rename the .env.example file to .env and add database credentials to this file and save it. Make sure to create a new database for this project in Mysql.
- Run the migrations to create Db schema.
```
$ php artisan migrate
```
- run the built in web server
```
$ php artisan serve
```

- open the browser and point to url [http://localhost:8000](http://localhost:8000).OPtionally you can use any other port if port 8000 is busy by specifiying the --port option
```
$ php artisan serve --port=8080
```
- Application is ready