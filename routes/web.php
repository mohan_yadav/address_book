<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace

    Route::get('/', function () {
        return view('home');
    });

    // Address Routes
    Route::get('/address', 'AddressController@index')->name('list_address');
    Route::get('/address/add_new', 'AddressController@create')->name('add_new_address');
    Route::post('/address/add_new', 'AddressController@store')->name('save_address');
    Route::get('/address/update/{address}', 'AddressController@edit')->middleware('can:update,address')->name('edit_address');
    Route::post('/address/update/{address}', 'AddressController@update')->middleware('can:update,address')->name('update_address');
    Route::get('/address/delete/{address}', 'AddressController@destroy')->middleware('can:update,address')->name('delete_address');


    // Update User Profile Routes
    Route::get('/address/update_profile', 'UserController@edit_profile')->name('edit_profile');
    Route::post('/address/update_profile', 'UserController@_profile')->name('update_profile');

});
