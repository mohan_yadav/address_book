<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'contact_person_name',
        'contact_person_number',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'pin_code',
        'city',
        'state',
        'country'
    ];

    /**
     * Get the user who has added this address.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }
}
