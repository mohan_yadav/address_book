<?php

namespace App\Policies;

use App\Address;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddressPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given Address can be updated by the user.
     *
     * @param  \App\User $user
     * @param Address $address
     * @return bool
     * @internal param Address $post
     */
    public function update(User $user, Address $address)
    {
        return $user->id === $address->user_id;
    }
}
