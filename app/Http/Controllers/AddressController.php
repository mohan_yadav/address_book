<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\AddressRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AddressController extends Controller
{

    public $data = ['route' => 'address_book'];

    /**
     * Display a listing of the Address.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['addresses'] = Auth::user()->addresses()->get();
        return view('address.index', $this->data);
    }

    /**
     * Show the form for creating a new Address.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.add_new', $this->data);
    }

    /**
     * Store a newly created address in storage.
     *
     * @param AddressRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressRequest $request)
    {
        $address = Address::create($request->all());
        $address->user_id = $request->user()->id;
        $address->save();
        return redirect()->route('list_address')->with('status', 'Address Saved !!');
    }


    /**
     * Show the form for editing the specified address.
     *
     * @param  Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        $this->data['address'] = $address;
        return view('address.add_new', $this->data);
    }

    /**
     * Update the specified address in storage.
     *
     * @param AddressRequest|Request $request
     * @param  Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(AddressRequest $request, Address $address)
    {
        $address->update($request->all());
        return redirect()->route('list_address')->with('status', 'Address Updated !!');
    }

    /**
     * Remove the specified address from storage.
     *
     * @param Address $address
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Address $address)
    {
        $address->delete();
        return redirect()->route('list_address')->with('status', 'Address Deleted !!');
    }
}
