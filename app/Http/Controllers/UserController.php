<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     *
     * This data is passed to the Views
     */
    public $data = ['route' => 'update_profile'];

    /**
     * Display the Profile Update form to the User
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit_profile(Request $request)
    {
        $this->data['user'] = $request->user();
        $this->data['userAddressList'] = $request->user()->addresses()->get()->pluck('title','id')->toArray();
        //dd($this->data['userAddressList']);
        return view('profile.update', $this->data);
    }

    /**
     * Save the updated profile information to database.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_profile(Request $request){
        $user = $request->user();
        //dd($request->all());
        $user->update($request->all());
        return redirect()->route('list_address')->with('status', 'Profile Updated !!');
    }

}
