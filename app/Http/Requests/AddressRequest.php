<?php

namespace App\Http\Requests;

use App\Address;
use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the address request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'contact_person_name' => 'required|max:50',
            'contact_person_number' => 'required|digits:10|numeric',
            'address_line_1' => 'required|max:50',
            'pin_code' => 'required|numeric|digits:6',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
        ];
    }
}
